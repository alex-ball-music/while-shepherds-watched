\version "2.20.0"

\language "english"
theStaffSize = #19
\include "ajb-common.ly"
\include "articulate.ly"

\header {
  title = "While Shepherds Watched"
  subtitle = "Tune: ‘Chestnut Major’"
  composer = "English Traditional arr. Alex Ball"
  poet = "Nahum Tate"
  copyright = \markup { \center-column {
    "© Alex Ball 2010–9. Released under a Creative Commons Attribution Non-Commercial 4.0 International"
    "Licence: http://creativecommons.org/licenses/by-nc/4.0/" } }
  tagline = \markup { \italic { First performed by the St George Singers and the choir of St Peter’s Church, Filton, Christmas 2010 } }
}

#(define-public
  (parentheses-item::calc-parenthesis-left-stencils grob)
  (let* ((font (ly:grob-default-font grob))
         (lp (ly:font-get-glyph font "accidentals.leftparen")))
  (list lp empty-stencil)))

#(define-public
  (parentheses-item::calc-parenthesis-right-stencils grob)
  (let* ((font (ly:grob-default-font grob))
         (rp (ly:font-get-glyph font "accidentals.rightparen")))
  (list empty-stencil rp)))

pLeft = \once \override ParenthesesItem.stencils = #parentheses-item::calc-parenthesis-left-stencils

pRight = {
  \once \override ParenthesesItem.stencils = #parentheses-item::calc-parenthesis-right-stencils
  \once \override ParenthesesItem.padding = #1
}

\layout {
  \context { \Score
    \override RehearsalMark.stencil
      = #(make-stencil-boxer 0.1 0.25 ly:text-interface::print)
  }
  \context { \Staff
    \override ParenthesesItem.font-size = #2
  }
}

global = {
  \key g \major
  \time 2/2
  \tempo "Cheerfully" 2 = 72
  \partial 4
}

% Control

control = {
  s4
  s1*3
  s2. \bar "||" \mark\default
  s4
  s1*19
  s2. \bar "||" \mark\default
  s4
  s1*3
  s2. \bar "||" \mark\default
  s4
  s1*19
  s2. \bar "||" \mark\default
  s4
  s1*3
  s2. \bar "||" \mark\default
  s4
  s1*23
  s2. \bar "|."
}

% Parts

soprano = \relative c'' {
  \global\dynamicUp
  % intro
  r4
  R1*3
  r2.
  % verse 1
  \break
  g4\mp
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d2. g,4\mf
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d2. d4\f
  e4 c\mp c4. e8
  d4 b b4. d8
  c4 b a g
  fs2 d\f
  e4. fs8 g4 a
  b4 c d2 ~
  d1 ~
  d2 r4 c4
  b4 b a a
  g2 fs'4\rest d8( c)
  b4. b8 a4 a
  g2.
  % bridge
  \break
  r4
  R1*3
  r2.
  % verse 2
  g8( a
  b8 a b c d4 a
  g4 a b) d(
  e4 d ~ d c8 b
  a1)
  r4 b8( c d4 a
  g4 a b) d(
  e4 d4 ~ d c8 b
  a2.) b4(
  c2 a2)
  b8( a b c d2)
  e4( d c cs
  d2) b2(
  g4 c b a
  g4 c a2)
  d2( c
  bf2 a
  g4) r4 r4 d'8( c)
  b4 b a a
  g2 g4 fs
  g2.
  % bridge
  r4
  R1*3
  r2.
  % verse 3
  g4
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d4. d8 f8( e) d4
  r4 d ~ d c
  b4 a g4. fs8
  g4 a b c
  d2. \once\override DynamicText.extra-offset = #'( -2.2 . -1.5) d4\ff
  e4 c c4. e8
  d4 b b4. d8
  c4\> b a g\!
  fs2 d
  e4.\< fs8 g4 a
  b4 c\! d2 ~
  d1 ~
  d2 r4 c4
  b4 b a a
  g2 fs'4\rest d8( c)
  b4. b8 a4 a
  g2.
  % coda
  d'8(\p c)
  b4 g a d8( c)
  b4 g a d8(\ff c)
  b4. b8 c4 d
  <d g>2.
}

alto = \relative c' {
  \global
  % intro
  r4
  R1*3
  r2.
  % verse 1
  d4
  d4 g a fs
  g4 fs e d
  e4 fs d g
  g4( e fs) d
  d4 g a fs
  g4 fs e d
  e4 fs d g
  g4( fs8 e fs4) d
  e4 fs g a
  b4 g g fs
  a4 g e e
  e2 d2
  e4. fs8 g4 a
  b4 c d2 ~
  d1 ~
  d2 r2
  r2. d8( c)
  b4 b a a
  g2 g4 fs
  g2.
  % bridge
  r4
  R1*3
  r2.
  % verse 2
  d4( ~
  d4 g a fs
  e4 fs e) d(
  e4 fs d g ~
  g4 e d2)
  r4 g( a fs
  e4 fs e) d(
  e4 fs d g ~
  g4 fs8 e fs4) g( ~
  g4 fs e fs
  g2.) fs4(
  a4 b a e
  e2) d2(
  e2. d4
  g2 fs2)
  d2( e2
  f2 fs2
  g4) r4 r4 d'8( c)
  b4 b a a
  g2 g4 fs
  g2.
  % bridge
  r4
  R1*3
  r2.
  % Verse 3
  d4
  d4 g a fs
  g4 fs e d
  e4 fs d g
  g4 e d8( e) fs4
  r4 g( a) fs
  g4 fs e d
  e4 fs d g
  g4( fs8 e fs4) d'
  e4 c c4. e8
  d4 b b4. fs8
  a4 g e e
  e2 d2
  e4. fs8 g4 a
  b4 g fs2 ~
  fs1 ~
  fs2 r2
  r4 r4 r4 d'8( c)
  b4 b a a
  g2 g4 fs
  g2.
  % coda
  fs4
  g4 g fs fs
  g4 g fs fs
  g4 g a g8( fs)
  g2.
}

tenor = \relative c' {
  \global
  % intro
  r4
  R1*3
  r2.
  % verse 1
  b4
  b4 b d a
  b c b b
  b d d c8( b)
  a2. b4
  b4 b d a
  b c b b
  b d d c8( b)
  a2. d4
  e4 a, a a
  g4 d' d d
  e4 d c a
  a2 d,
  e4. fs8 g4 a
  b4 c d2 ~
  d1 ~
  d2 r4 c4
  b4 b a a
  g2 e'4\rest d8( c)
  b4. b8 a4 a
  g2.
  % bridge
  r4
  R1*3
  r2.
  % verse 2
  g4
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d4. d8 b8( c) d4
  r4 d4 ~ d c
  b4 a g4. fs8
  g4 a b c
  d2. d4
  e4 c c4. e8
  d4 b b4. d8
  c4 b a g
  fs2 d
  e4. fs8 g4 a
  b4 c d2 ~
  d1 ~
  d2 r4 c4
  b4 b a a
  g2 r4 d'8( c)
  b4. b8 a4 a
  g2.
  % bridge
  r4
  R1*3
  r2.
  % verse 3
  b4
  b4 b d a
  b c b b
  b d d c8( b)
  a4. a8 g4 a
  r4 b( d) a
  b c b b
  b d d c8( b)
  a2. d4
  e4 c c4. e8
  d4 b b4. d8
  e4 d c a
  a2 d,
  e4. fs8 g4 a
  b4 c d2 ~
  d1 ~
  d2 r4 c4
  b4 b a a
  g2 e'4\rest d8( c)
  b4. b8 a4 a
  g2.
  % coda
  a4
  b4 b d c
  b4 b d c
  b4 b e c
  b2.
}

bass = \relative c' {
  \global
  % intro
  r4
  R1*3
  r2.
  % verse 1
  g4
  g4 g fs d
  g4 d e b
  e4 d g e
  d2. d4
  g4 g fs d
  g4 d e b
  b'4 a g e
  d2. d4
  e4 d e fs
  g4 a b b,
  a4 b c cs
  d2 d
  e4. fs8 g4 a
  b4 c d2
  d,2 fs
  a2 d,
  g4 r4 r4 d'8( c)
  b4 b a a
  g2 g4 fs
  g2.
  % bridge
  r4
  R1*3
  r2.
  % verse 2
  g4
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d4. d8 b8( c) d4
  r4 d4 ~ d c
  b4 a g4. fs8
  g4 a b c
  d2. d4
  e4 c c4. e8
  d4 b b4. d8
  c4 b a g
  fs2 d
  e4. fs8 g4 a
  b4 c d2 ~
  d1 ~
  d2 r4 c4
  b4 b a a
  g2 r4 d'8( c)
  b4. b8 a4 a
  g2.
  % bridge
  r4
  R1*3
  r2.
  % verse 3
  g4
  g4 g fs d
  g4 d e b
  e4 d g e
  d4. d8 b8( c) d4
  r4 g( fs) d
  g4 d e b
  b'4 a g e
  d2. d'4
  e4 c c4. e8
  d4 b b4. b,8
  a4 b c cs
  d2 d2
  e4. fs8 g4 a
  g e d2
  d'2 a
  d,2 fs
  g4 r4 r4 d'8( c)
  b4 b a a
  g2 g4 fs
  g2.
  % coda
  d4
  g4 g d d
  g4 g d d
  g4 e c d
  g,2.
}

sopranoVerse = {
  \lyricmode {
    % Lyrics follow here.
    \repeat "unfold" 14 { \sh }
    \repeat "unfold" 14 { \sh }
    \repeat "unfold" 14 { \sh }
    \repeat "unfold" 8 { \sh }
    to you and all man -- kind,
    to you and all man -- kind.’
  }
\set stanza = \markup { \dynamic "mf" " S.A. " }
  \lyricmode {
    Ah. __
    Ah. __
    Ah. __
    Ah. __
    Ah. __
    Ah. __
    Ah. __
    Ah. __
    Ah. __
    and in a man -- ger laid, man -- ger laid.’

    \repeat "unfold" 14 { \sh }
    \repeat "unfold" 14 { \sh }
    \repeat "unfold" 14 { \sh }
    \repeat "unfold" 8 { \sh }
    be -- gin and ne -- ver cease,
    be -- gin and ne -- ver cease,
  }
}

altoVerse = \lyricmode {
\set stanza = "1. "
  While shep -- herds watched their flocks by night,
  all seat -- ed on the ground,
  the an -- gel of the Lord came down,
  and glo -- ry shone a -- round.
  ‘Fear not,’ said he "(for" migh -- ty dread
  had seized their troub -- led mind)
  ‘Glad ti -- dings of great joy I bring __
  to you and all man -- kind, all man -- kind.’

  \repeat "unfold" 5 { \sh }
  Ah. __
  Ah. __
  ""
  \repeat "unfold" 9 { \sh }

  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 8 { \sh }
  be -- gin and ne -- ver cease,
  ne -- ver cease,

  be -- gin and ne -- ver,
  ne -- ver cease,
  be -- gin and ne -- ver cease.’
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 8 { \sh }
  to you and all man -- kind,
  to you and all man -- kind.’

\set stanza = "T.B. 2. "
  ‘To you in Da -- vid’s town this day
  is born of Da -- vid’s line
  a Sa -- viour who is Christ the Lord;
  and this shall be the sign.
  The heav’n -- ly babe you there shall find
  to hu -- man view dis -- played,
  all mean -- ly wrapped in swadd -- ling bands __
  and in a man -- ger laid,
  and in a man -- ger laid.’

\set stanza = "3. "
  Thus spake the se -- raph, and forth -- with
  ap -- peared a shi -- ning throng
  of an -- gels, prais -- ing God, who thus
  ad -- dressed their joy -- ful song:
  ‘All glo -- ry be to God on high,
  and to the earth be peace;
  good -- will hence -- forth from heav’n to men __
  be -- gin and ne -- ver cease,
  be -- gin and ne -- ver cease,
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 8 { \sh }
  joy I bring to you,
  to you and all man -- kind,
  all man -- kind.’

  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 6 { \sh }

  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 14 { \sh }
  \repeat "unfold" 8 { \sh }
  heav'n to men be -- gin,
  be -- gin and ne -- ver cease,
  ne -- ver cease,
}

rightOne = \relative c'' {
  \global
  % intro
  g4
  g4 d' d c
  b4 g a d8 c
  b4. b8 a4 g8 fs
  g2.
  % verse 1
  g4
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d2. g,4
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d2. d4
  e4 c c4. e8
  d4 b b4. d8
  <c a>4 <b g> <a e> <g e>
  fs2 d
  e4. fs8 g4 a
  b4 c d2 ~
  d1 ~
  d2. c4
  b4 b a d8 c
  b4 b a d8 c
  b4. b8 a4 a
  g2.
  % bridge
  d'8 c
  b4 g a d8 c
  b4 g a d8 c
  b4. b8 a4 g8 fs
  g2.
  % verse 2
  g4
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d4. d8 <f b,>8 <e c> d4
  d4 d4 ~ d c
  b4 a g4. fs8
  g4 a b c
  d2. d4
  e4 c c4. e8
  d4 b b4. d8
  <c a>4 <b g> <a e> <g e>
  fs2 d
  e4. fs8 g4 a
  b4 c d2
  <d a>2 <c g>2
  bf2 a4 c
  b4 b a d8 c
  b4 b a d8 c
  b4. b8 a4 a
  g2.
  % bridge
  d'8 c
  b4 g a d8 c
  b4 g a d8 c
  b4. b8 a4 g8 fs
  g2.
  % verse 3
  g4
  g4 d' d c
  b4 a g4. fs8
  g4 a b c
  d4. d8 f8 e d4
  g,4 d' d c
  b4 a g4. fs8
  g4 a b c
  d2. d4
  e4 c c4. e8
  d4 b b4. d8
  <c a>4 <b g> <a e> <g e>
  fs2 d
  e4. fs8 g4 a
  b4 c d2 ~
  d1 ~
  d2. c4
  b4 b a d8 c
  b4 b a d8 c
  b4. b8 a4 a
  g2.
  % coda
  d'8 c
  b4 g a d8 c
  b4 g a d8 c
  b4. b8 c4 d
  <d g>2.
}

rightTwo = \relative c' {
  \global
  % Intro
  d4\mf
  d4 g a fs
  g e fs d
  g g g d
  d2.
  % Verse 1
  d4\mp
  d4 g a fs
  g4 fs e d
  e4 fs d g
  g4 e fs d\mf
  d4 g a fs
  g4 fs e d
  e4 fs d g
  g4 fs8 e fs4 g\f
  g4 fs\mp g a
  b4 g g <fs d>
  e4 d c a
  <a e'>2 a2\f
  e'4 <e c> e d
  g4 g <fs a>2 ~
  <fs a>1 ~
  <fs a>2. <fs a>4
  <g b,>4 <g b,> g fs
  g2 r4 fs
  g4 g <g e> <fs c>4
  <d b>2.
  % bridge
  fs4\mf
  g4 g fs fs
  g4 g fs fs
  g4 g <g e> <d c>
  d2.
  % Verse 2
  d4
  d4 g a fs
  g4 fs e d
  e4 <fs d> d g
  g4 e g fs
  b4 g a fs
  g4 fs e d
  e4 fs d g
  g4 fs8 e fs4 g
  g4 fs g a
  g2. fs4
  e4 d c a
  <a e'>2 a2
  e'4 <e c> e d
  g4 g fs2
  d1
  d2. fs4
  <g b,>4 <g b,> g fs
  g2 r4 fs
  g4 g <g e> <fs c>4
  <d b>2.
  % bridge
  fs4
  g4 g fs fs
  g4 g fs fs
  g4 g <g e> <d c>
  d2.
  % Verse 3
  d4
  d4 g a fs
  g4 fs e d
  e4 fs d g
  g4 e d8 e fs4
  d4 g a fs
  g4 fs e d
  e4 fs d g
  g4 fs8 e fs4 g\ff
  g4 fs g a
  b4 g g <fs d>
  e4\> d c a\!
  <a e'>2 <a d>2
  e'4\< <e c> e d
  g4 g\! <fs a>2 ~
  <fs a>1 ~
  <fs a>2. <fs a>4
  <g b,>4 <g b,> g fs
  g2 r4 fs
  g4 g <g e> <fs c>4
  b,2.
  % coda
  fs'4\p
  g4 g fs fs
  g4 g fs fs\ff
  g4 g <g a e> g8 fs
  g2.
}

leftOne = \relative c' {
  \global
  % intro
  b4
  b4 b d a
  b4 c a a
  b b c c
  b2.
  % verse 1
  b4
  b4 b d a
  b c b b
  b d d c8( b)
  a2. b4
  b4 b d a
  b c b b
  b d d c8( b)
  a2. b4
  c4 a a a
  g4 d' d s
  s1
  s1
  g,4 s b a
  g4 c s2
  s1
  s1
  s2 d4 c
  b4 b d c
  b4 b s2
  s2.
  % bridge
  a4
  b4 b d c
  b4 b d c
  b4 b s2
  b2.
  % verse 2
  b4
  b8 a b c d4 a
  b4 <a c> b s4
  s2. c8 b
  a1
  s4 b8 c d4 a
  b4 <a c> b s
  e4 d ~ d c8 b
  a2. b4
  c a a a
  b8 a b c d2
  s1
  s1
  g,4 s b a
  g4 c a2
  s1
  s2 r4 a
  s2 d4 c
  b4 b d c
  b4 b s2
  s2.
  % bridge
  a4
  b4 b d c
  b4 b d c
  b4 b s2
  b2.
  % verse 3
  b4
  b4 b d a
  b c b b
  b d d c8( b)
  a4. a8 g4 a
  b4 b d a
  b c b b
  b d d c8( b)
  a2. b4
  c4 a a a
  g4 d' d s
  s1
  s1
  g,4 s b a
  g4 c s2
  s1
  s1
  s2 d4 c
  b4 b d c
  b4 b s2
  s2.
  % coda
  a4
  b4 b d c
  b4 b d c
  b4 b s c
  <b d>2.
}

leftTwo = \relative c' {
  \global
  % intro
  g4
  g4 g fs d
  g4 c, d fs
  g4 e c d
  g4 d g,
  % verse 1
  g'4
  g4 g fs d
  g4 d e b
  e4 d g e
  d2. d4
  g4 g fs d
  g4 d e b
  b'4 a g e
  d2. g4
  c,4 d e fs
  g4 a b \oneVoice b,
  a4 b c cs
  d2 b2 \voiceTwo
  c4 \oneVoice a \voiceTwo e' fs
  g e \oneVoice d2
  <d d,>2 <fs fs,>
  <a a,>2 <d, d,>
  g,2 \voiceTwo d'4 d
  g4 g d d
  g4 e \oneVoice c d
  g,4 d' g \voiceTwo
  % bridge
  d4
  g4 g d d
  g4 g d d
  g4 e \oneVoice c d \voiceTwo
  g4 d g,
  % verse 2
  g'4
  g8 fs g a fs4 d
  g4 d e \oneVoice b
  e4 d g \voiceTwo e
  d1 \oneVoice
  <g g,>4 \voiceTwo g fs d
  g4 d e \oneVoice b \voiceTwo
  b'4 a g e
  d2. g4
  c,4 d e fs
  g8 fs g a b4 b, \oneVoice
  a4 b c cs
  d2 b2 \voiceTwo
  c4 \oneVoice a \voiceTwo e' fs
  g e d2 \oneVoice
  <d d,>2 <e e,>
  <f f,>2 \voiceTwo <fs fs,>
  g,2 d'4 d
  g4 g d d
  g4 e \oneVoice c d
  g,4 d' g \voiceTwo
  % bridge
  d4
  g4 g d d
  g4 g d d
  g4 e \oneVoice c d \voiceTwo
  g4 d g,
  % verse 3
  g'4
  g4 g fs d
  g4 d e b
  e4 d g e
  d4. d8 b8 c d4
  g4 g fs d
  g4 d e b
  b'4 a g e
  d2. g4
  c,4 d e fs
  g4 a b \oneVoice b,
  a4 b c cs
  d2 b2 \voiceTwo
  c4 \oneVoice a \voiceTwo e' fs
  g e \oneVoice d2
  <d d,>2 <a a,>
  <d, d,>2 <fs fs,>
  <g g,>2 \voiceTwo d'4 d
  g4 g d d
  g4 e \oneVoice c d
  g,4 d' g \voiceTwo
  % coda
  d4
  g4 g d d
  g4 g d d
  g4 e \oneVoice c \voiceTwo d
  g4 d g,
}

choirPart = \new ChoirStaff <<
  \new Staff = "sa" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "S." "A." }
  } <<
    \new Voice = "control" { \oneVoice \control }
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
  >>
  \new Lyrics = "soprano-lyrics" \with {
    alignAboveContext = "sa"
  } \lyricsto "soprano" \sopranoVerse
  \new Lyrics = "alto-lyrics" \lyricsto "alto" \altoVerse
  \new Staff = "tb" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "T." "B." }
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \bass }
  >>
  \new Lyrics = "tenor-lyrics" \with {
    alignAboveContext = "tb"
  } \lyricsto "tenor" \tenorVerse
  \new Lyrics = "bass-lyrics" \lyricsto "bass" \bassVerse
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Acc."
} <<
  \new Staff = "right" \with {
    midiInstrument = "acoustic grand"
%    \override VerticalAxisGroup.minimum-Y-extent = #'(-4 . 5)
  } << \rightOne \\ \rightTwo >>
  \new Staff = "left" \with {
    midiInstrument = "acoustic grand"
  } << { \clef bass \leftOne } \\ { \clef bass \leftTwo } >>
>>

trumpet = \transpose c d \relative c'' {
  \transposition bf
  \compressFullBarRests
  \global
  % intro
  g4\mf
  g4 d' d c
  b4 g a \breathe d8( c)
  b4. b8 a4 g8( fs)
  g2.
  % verse 1
  r4
  R1 * 19
  r2.
  % bridge
  \break
  d'8( c)
  b4 g a d8( c)
  b4 g a \breathe d8( c)
  b4. b8 a4 g8( fs)
  g2.
  % verse 2
  r4
  R1 * 19
  r2.
  % bridge
  \break
  d'8( c)
  b4 g a \breathe b8( c)
  d4. e8 fs8( e d c)
  b4. b8 a4 g8( fs)
  g2.
  % verse 3
  r4
  R1 * 7
  r2. b4\mf
  c8( b c d) e4 c
  b8( a b c) d4.\breathe b8
  a4\> b c a\!
  d2 \breathe d2(\< ~
  d8 c b a) g4 e'4( ~
  e8 d c b) a2\mf \breathe
  d1\trill\mp\<( ~
  d8\mf e fs e) d4 \breathe d8( c)
  b4 g a fs
  g8( a b c) d4 \breathe d8( e)
  d4 c8( b) c( b) a4
  b2.
  % coda
  r4
  R1
  r2. fs4\f
  g8( a) b( c) d8( e)
  << { \voiceOne fs4 g2. } \new Voice {\voiceTwo\small \pLeft\parenthesize d4 \pRight\parenthesize d2. } >>
}

trumpetPart = \new Staff \with {
  instrumentName = \markup \center-column { "Trumpet" \concat { "(B" \translate #'(0 . 0.5){ \tiny\flat } ")" } }
  midiInstrument = "trumpet"
} <<
  \new Voice = "control" { \oneVoice \control }
  \new Voice = "trumpet" { \oneVoice  \trumpet }
>>


\book {
  % PDF
  \paper {
    page-count = #5
  }
  \score {
    <<
      \choirPart
      \pianoPart
    >>
    \layout {
      \context {
        \Score
        \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/8)
      }
    }
  }
  % MIDI
  \score { \articulate
    <<
      \trumpetPart
      \choirPart
      \pianoPart
    >>
    \midi {
    }
  }
}

\book {
  \bookOutputSuffix "trumpet"
  \score {
    <<
      \trumpetPart
    >>
    \layout {}
  }
}
