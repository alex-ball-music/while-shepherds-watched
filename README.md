# While shepherds watched: Chestnut Major

This is an arrangement of one of the many tunes for ‘While shepherds watched’
that have fallen out of favour since Winchester New cornered the market.

The tune is a major variant of the same basic melody, Chestnut, that underlies
‘Come all ye worthy gentlemen’, ‘God rest ye merry, gentlemen’, ‘Unto us is born
a son’, and probably others. There is an arrangement of it in the *Oxford Book
of Carols*, but personally I found that one a bit pedestrian so wrote this one
instead.

The trumpet part was added in 2019.

## Summary information

  - *Voicing:* SATB (optional split in Soprano on last chord)

  - *Notes on ambitus:* S goes up to G; T goes up to E, AB go up to D.

  - *Instrumentation:* Piano or organ; optional trumpet.

  - *Approximate performance length:* 2:00

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
     Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>

## Files

This repository contains the [Lilypond](http://lilypond.org) source code. To
compile it yourself, you will also need my [house style
files](https://gitlab.com/alex-ball-music/ajb-lilypond).

For PDF and MIDI downloads, see the [Releases
page](https://gitlab.com/alex-ball-music/while-shepherds-watched/-/releases).
